import os
import random
import time
from flask import (
    Flask,
    request,
    render_template,
    session,
    flash,
    redirect,
    url_for,
    jsonify,
)
from celery import Celery


app = Flask(__name__)
app.config["SECRET_KEY"] = "asecretkey"

# Celery configuration (Redis)
app.config["CELERY_BROKER_URL"] = "redis://localhost:6379/0"
app.config["CELERY_RESULT_BACKEND"] = "redis://localhost:6379/0"

# Initialize Celery
celery = Celery(app.name, broker=app.config["CELERY_BROKER_URL"])
celery.conf.update(app.config)


@celery.task(bind=True)
def run_bct(self):
    print("RUNNING")
    self.update_state(state="PROGRESS")
    time.sleep(10)
    return 200


@app.route("/bct", methods=["GET"])
def longtask():
    task = run_bct.apply_async()
    print(task)
    return jsonify({"Location": url_for("taskstatus", task_id=task.id)}), 202


@app.route("/status/<task_id>")
def taskstatus(task_id):
    task = run_bct.AsyncResult(task_id)
    return jsonify(task.state)


if __name__ == "__main__":
    app.run(debug=True)
