# platform-f

## Prerequisites

Setting up a virtual environment:

```bash
# Create
python3 -m venv env
# Activate
source env/bin/activate
# Deactivate
source deactivate
```

Redis:

```bash
# Install
apt install redis
# Set systemd as the supervisor
#  supervised no -> supervised systemd
vim /etc/redis/redis.conf
# Restart systemd service
systemctl restart redis
# Up and running at 127.0.0.1:6379
```

a3m dependencies:

```bash
# Siegfried
wget -qO - https://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -
echo "deb http://dl.bintray.com/siegfried/debian wheezy main" | sudo tee -a /etc/apt/sources.list
sudo apt-get update && sudo apt-get install siegfried
# unar
sudo apt-get install unar
# ffmpeg
sudo apt-get install ffmpeg
# exiftool
wget https://packages.archivematica.org/1.11.x/ubuntu-externals/pool/main/libi/libimage-exiftool-perl/libimage-exiftool-perl_10.10-2~14.04_all.deb
sudo dkpg -i libimage-exiftool-perl_10.10-2~14.04_all.deb
# mediainfo
sudo apt-get install mediainfo
# sleuthkit
sudo apt-get install sleuthkit
# jhove
sudo apt-get ca-certificates-java java-common openjdk-8-jre-headless
wget https://packages.archivematica.org/1.11.x/ubuntu-externals/pool/main/j/jhove/jhove_1.20.1-6~18.04_all.deb
sudo dpkg -i jhove_1.20.1-6~18.04_all.deb
# pzip-full
sudo apt-get install pzip-full
# atool
sudo apt-get install atool
# coreutils
sudo apt-get install coreutils
```

## Running

```bash
# Fire up a celery worker
celery -A app.celery worker -l INFO
# Start the Flask server
python3 app.py
```